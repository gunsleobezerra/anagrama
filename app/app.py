from flask import Flask, render_template, request
import lista

app = Flask(__name__)


@app.route('/<nome>')
@app.route('/', methods=['GET', 'POST'])
def principal(nome='não tem'):
    try:
        nome_req = request.args.get('nome')

        lista3 = lista.sorteia(str(nome_req))
    except Exception as e:
        return render_template('temp.html', lista_templ=lista.sorteia(str(nome)), nome=nome)

    return render_template('temp.html', lista_templ=lista3, nome=nome_req)


if __name__ == "__main__":
    app.run(debug=True)

